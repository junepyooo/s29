const express = require('express')
const app = express()
const port = 3000

app.get('/home', (req, res) => {
    res.send('This is home')
})

app.listen(port, () => {
    console.log(`Server running at port http://localhost:${port}`)
})

let users = [];

app.get('/users', (req, res) => {
    res.send(users)
})

app.post('/signup', (req, res) => {
    console.log(req.body)
    //check if the contents of re.body is empty or not
    if (req.body.lastName !== '' && req.body.firstName !== '' && req.body.email !== '') {
        //if the req.body is not empty
        //push it in our users array
        users.push(req.body)

        //This will send a response back to the client/Postman
        res.send(`User ${req.body.username} successfully registered!`)
    } else {
        res.send('Please input BOTH username and password')
    }
})