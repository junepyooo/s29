/*
1. npm init
2. npm install express
*/

// Use the "require" directive to load the express module/package
// A "module" is a software component or part of a program that contains one or more routines
// This is used to get the contents of the express package to be used by our application
// It also allows us access to methods and functions that will allow us to easily create a server

const express = require('express');

// Create an application using express
// This creates an express application and stores this in a constant called app
// In layman's terms, app is our server
const app = express();

//For our application server to run, we need a port to listen
const port = 3000;

//Mock Database
let users = [];

//Allows your app to read json data
//middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// GET route
app.get('/', (req, res) => {
    res.send('Hello World!')
})

//Another GET Request
app.get('/hello', (req, res) => {
    res.send('Hello from the /hello endpoint')
});

//This route expects to receive a POST request at the "/hello" URI
app.post('/hello', (req, res) => {
    res.send(Hello there ${ req.body.firstName } ${ req.body.lastName });
});
/*
Sign up will take in username =  and 	password =
                    req.body.username	req.body.password
*/
//endpoint
app.post('/signup', (req, res) => {

})

// Tells our server to listen to the port
// If the port is accessed, we can run the server
// Returns a message to confirm that the server is running in the terminal
app.listen(port, () => console.log(Server running at port ${ port }))